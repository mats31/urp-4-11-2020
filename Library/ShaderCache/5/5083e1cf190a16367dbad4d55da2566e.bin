<Q                           l  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _ProjectionParams;
    float4 _ScreenParams;
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_TARGET0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_CameraColorTexture [[ sampler (0) ]],
    texture2d<float, access::sample > _CameraColorTexture [[ texture(0) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    float4 u_xlat1;
    bool u_xlatb1;
    float4 u_xlat2;
    float4 u_xlat3;
    bool u_xlatb3;
    float4 u_xlat4;
    bool2 u_xlatb4;
    float3 u_xlat5;
    float3 u_xlat6;
    float3 u_xlat9;
    float3 u_xlat10;
    bool2 u_xlatb10;
    float u_xlat12;
    float2 u_xlat13;
    float u_xlat15;
    float u_xlat18;
    float u_xlat21;
    bool u_xlatb21;
    u_xlat0.xyz = input.TEXCOORD0.yyy * FGlobals.hlslcc_mtx4x4unity_MatrixVP[1].xyw;
    u_xlat0.xyz = fma(FGlobals.hlslcc_mtx4x4unity_MatrixVP[0].xyw, input.TEXCOORD0.xxx, u_xlat0.xyz);
    u_xlat0.xyz = fma(FGlobals.hlslcc_mtx4x4unity_MatrixVP[2].xyw, input.TEXCOORD0.zzz, u_xlat0.xyz);
    u_xlat0.xyz = u_xlat0.xyz + FGlobals.hlslcc_mtx4x4unity_MatrixVP[3].xyw;
    u_xlat1.xz = u_xlat0.xz * float2(0.5, 0.5);
    u_xlat0.x = u_xlat0.y * FGlobals._ProjectionParams.x;
    u_xlat1.w = u_xlat0.x * 0.5;
    u_xlat0.xy = u_xlat1.zz + u_xlat1.xw;
    u_xlat0.xy = u_xlat0.xy / u_xlat0.zz;
    u_xlat0.xy = u_xlat0.xy * FGlobals._ScreenParams.xy;
    u_xlat12 = float(1.0);
    u_xlat18 = float(0.0);
    while(true){
        u_xlatb1 = u_xlat18>=2.0;
        if(u_xlatb1){break;}
        u_xlat1.x = u_xlat18 * 1.57079637;
        u_xlat2.x = cos(u_xlat1.x);
        u_xlat1.x = sin(u_xlat1.x);
        u_xlat1.xy = u_xlat1.xx * float2(0.0, -1.0);
        u_xlat1.xy = fma(u_xlat2.xx, float2(1.0, 0.0), u_xlat1.xy);
        u_xlat2.x = dot(u_xlat1.xy, u_xlat1.xy);
        u_xlat2.x = rsqrt(u_xlat2.x);
        u_xlat2 = u_xlat1.xyxy * u_xlat2.xxxx;
        u_xlat1.zw = (-u_xlat1.yy);
        u_xlat13.x = dot(u_xlat1.xz, u_xlat1.xw);
        u_xlat13.x = rsqrt(u_xlat13.x);
        u_xlat1.xy = u_xlat1.yx * float2(-1.0, 1.0);
        u_xlat1.xy = u_xlat13.xx * u_xlat1.xy;
        u_xlat13.y = u_xlat12;
        u_xlat13.x = -16.0;
        while(true){
            u_xlatb3 = 16.0<u_xlat13.x;
            if(u_xlatb3){break;}
            u_xlat3 = fma(u_xlat2, u_xlat13.xxxx, u_xlat0.xyxy);
            u_xlatb4.xy = (u_xlat3.wz<float2(0.0, 0.0));
            u_xlatb4.x = u_xlatb4.y || u_xlatb4.x;
            u_xlatb10.xy = (FGlobals._ScreenParams.xy<u_xlat3.zw);
            u_xlatb4.x = u_xlatb10.x || u_xlatb4.x;
            u_xlatb4.x = u_xlatb10.y || u_xlatb4.x;
            if(u_xlatb4.x){
                u_xlat4.xy = u_xlat13.yx + float2(0.0, 2.0);
                u_xlat13.xy = u_xlat4.yx;
                continue;
            }
            u_xlat4 = u_xlat3.zwzw + float4(1.0, 0.0, -1.0, -0.0);
            u_xlat4 = u_xlat4 / FGlobals._ScreenParams.xyxy;
            u_xlat5.xyz = _CameraColorTexture.sample(sampler_CameraColorTexture, u_xlat4.xy).xyz;
            u_xlat4.x = dot(u_xlat5.xyz, float3(0.212599993, 0.715200007, 0.0722000003));
            u_xlat10.xyz = _CameraColorTexture.sample(sampler_CameraColorTexture, u_xlat4.zw).xyz;
            u_xlat10.x = dot(u_xlat10.xyz, float3(0.212599993, 0.715200007, 0.0722000003));
            u_xlat4.x = (-u_xlat10.x) + u_xlat4.x;
            u_xlat3 = u_xlat3 + float4(0.0, 1.0, -0.0, -1.0);
            u_xlat3 = u_xlat3 / FGlobals._ScreenParams.xyxy;
            u_xlat5.xyz = _CameraColorTexture.sample(sampler_CameraColorTexture, u_xlat3.xy).xyz;
            u_xlat3.x = dot(u_xlat5.xyz, float3(0.212599993, 0.715200007, 0.0722000003));
            u_xlat9.xyz = _CameraColorTexture.sample(sampler_CameraColorTexture, u_xlat3.zw).xyz;
            u_xlat9.x = dot(u_xlat9.xyz, float3(0.212599993, 0.715200007, 0.0722000003));
            u_xlat4.y = (-u_xlat9.x) + u_xlat3.x;
            u_xlat3.xy = u_xlat4.xy * float2(0.5, 0.5);
            u_xlat15 = dot(u_xlat3.xy, u_xlat3.xy);
            u_xlat21 = sqrt(u_xlat15);
            u_xlatb21 = u_xlat21<0.00999999978;
            if(u_xlatb21){
                u_xlat4.xy = u_xlat13.yx + float2(0.0, 2.0);
                u_xlat13.xy = u_xlat4.yx;
                continue;
            }
            u_xlat15 = rsqrt(u_xlat15);
            u_xlat3.xy = float2(u_xlat15) * u_xlat3.xy;
            u_xlat3.x = dot(u_xlat1.xy, u_xlat3.xy);
            u_xlat3.x = log2(abs(u_xlat3.x));
            u_xlat3.x = u_xlat3.x * 10.0;
            u_xlat3.x = exp2(u_xlat3.x);
            u_xlat13.y = fma((-u_xlat3.x), 0.03125, u_xlat13.y);
            u_xlat13.x = u_xlat13.x + 2.0;
        }
        u_xlat12 = u_xlat13.y;
        u_xlat18 = u_xlat18 + 1.0;
    }
    u_xlat1.xy = u_xlat0.xy / FGlobals._ScreenParams.xy;
    u_xlat1.xyz = _CameraColorTexture.sample(sampler_CameraColorTexture, u_xlat1.xy).xyz;
    u_xlat2.xyz = (-u_xlat1.xyz) + float3(1.0, 1.0, 1.0);
    u_xlat1.xyz = fma(u_xlat2.xyz, float3(0.5, 0.5, 0.5), u_xlat1.xyz);
    u_xlat0.xy = fma((-FGlobals._ScreenParams.xy), float2(0.5, 0.5), u_xlat0.xy);
    u_xlat0.x = dot(u_xlat0.xy, u_xlat0.xy);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat0.x = u_xlat0.x / FGlobals._ScreenParams.x;
    u_xlat6.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = fma((-u_xlat6.x), u_xlat0.x, 1.0);
    u_xlat6.xyz = float3(u_xlat12) * u_xlat1.xyz;
    output.SV_TARGET0.xyz = u_xlat6.xyz * u_xlat0.xxx;
    output.SV_TARGET0.w = 1.0;
    return output;
}
                              FGlobals`         _ProjectionParams                            _ScreenParams                           unity_MatrixVP                                 _CameraColorTexture                   FGlobals           