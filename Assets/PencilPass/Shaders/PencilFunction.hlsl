#define PI2 6.28318530717959

#define RANGE 16.
#define STEP 2.
#define ANGLENUM 4.

// Grayscale mode! This is for if you didn't like drawing with colored pencils as a kid
//#define GRAYSCALE

// Here's some magic numbers, and two groups of settings that I think looks really nice. 
// Feel free to play around with them!

#define MAGIC_GRAD_THRESH 0.01

// Setting group 1:
/*#define MAGIC_SENSITIVITY     4.
#define MAGIC_COLOR           1.*/

// Setting group 2:
#define MAGIC_SENSITIVITY     10.
#define MAGIC_COLOR           0.5

TEXTURE2D(_CameraColorTexture);
SAMPLER(sampler_CameraColorTexture);
float4 _CameraColorTexture_TexelSize;

TEXTURE2D(_CameraDepthTexture);
SAMPLER(sampler_CameraDepthTexture);

float4 getCol(float2 pos)
{
    float2 uv = pos / _ScreenParams.xy;
    return SAMPLE_TEXTURE2D(_CameraColorTexture, sampler_CameraColorTexture, uv);
}

float getVal(float2 pos)
{
    float4 c=getCol(pos);
    return dot(c.xyz, float3(0.2126, 0.7152, 0.0722));
}

float2 getGrad(float2 pos, float eps)
{
   	float2 d=float2(eps,0);
    return float2(
        getVal(pos+d.xy)-getVal(pos-d.xy),
        getVal(pos+d.yx)-getVal(pos-d.yx)
    )/eps/2.;
}

void pR(inout float2 p, float a) {
	p = cos(a)*p + sin(a)*float2(p.y, -p.x);
}

void Pencil_float(
    float2 UV,
    float2 _Screen,
    float2 _Resolution,
    out float4 Out
)
{
    float2 pos = _Screen;
    float weight = 1;

    float4 test = float4(0, 0, 0, 0);

    for (float j = 0; j < ANGLENUM; j+=1)
    {
        float2 dir = float2(1, 0);
        pR(dir, j * PI2 / (2. * ANGLENUM));

        float2 grad = float2(-dir.y, dir.x);
        for (float i = -RANGE; i <= RANGE; i += STEP)
        {
            float2 pos2 = pos + normalize(dir)*i;
            
            // video texture wrap can't be set to anything other than clamp  (-_-)
            if (pos2.y < 0. || pos2.x < 0. || pos2.x > _Resolution.x || pos2.y > _Resolution.y)
                continue;
            
            float2 g = getGrad(pos2, 1.);
            if (length(g) < MAGIC_GRAD_THRESH)
                continue;
            
            weight -= pow(abs(dot(normalize(grad), normalize(g))), MAGIC_SENSITIVITY) / floor((2. * RANGE + 1.) / STEP) / ANGLENUM;

            test.r += 0.01;
        }
    }

#ifndef GRAYSCALE
    float4 col = getCol(pos);
#else
    float4 col = float4(getVal(pos));
#endif
    
    float4 background = lerp(col, float4(1, 1, 1, 1), MAGIC_COLOR);
    
    // I couldn't get this to look good, but I guess it's almost obligatory at this point...
    /*float distToLine = absCircular(fragCoord.y / (_Resolution.y/8.));
    background = lerp(float4(0.6,0.6,1,1), background, smoothstep(0., 0.03, distToLine));*/
    
    
    // because apparently all shaders need one of these. It's like a law or something.
    float r = length(pos - _Resolution.xy*.5) / _Resolution.x;
    float vign = 1. - r*r*r;
    
    // float4 a = texture(iChannel1, pos/_Resolution.xy);
    // fragColor = vign * lerp(float4(0), background, weight) + a.xxxx/25.;

    Out = vign * lerp(float4(0, 0, 0, 0), background, weight);
    // Out = float4(_Resolution, 0, 1);
    // Out = float4(UV, 0, 1);

    // Out = SAMPLE_TEXTURE2D(_CameraColorTexture, sampler_CameraColorTexture, UV);
    // Out = float4(0, 0, 0, 0);
    // Out = test;
}